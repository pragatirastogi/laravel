<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Redirect;
use App\DB;

use App\User;
use App\Csvdata;
use App\Salary;
use App\Monthlysalary;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$users = User::where('email', '!=', 'admin@test.com')
            ->leftjoin('salaries', 'users.id', '=', 'salaries.user_id')
            ->select('users.*', 'salaries.basic', 'salaries.hra', 'salaries.da')
            ->paginate(50);
		return view('user.index', ['users' => $users]);
    }

    public function test(){
		$month = 'feb';
		$n = date('m', strtotime($month));
		echo cal_days_in_month(CAL_GREGORIAN,$n,2016);
	}
}
