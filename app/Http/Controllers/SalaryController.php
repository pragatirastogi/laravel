<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DB;
use App\User;
use App\Csvdata;
use App\Salary;
use App\Monthlysalary;
class SalaryController extends Controller
{
    public function salaryStructrue(Request $r)
    {
		$method = $r->method();
		if ($method =="POST" || $method =="post"){
			$csv_file = $r->file('csv_file');
			$file_name = 'salary_structure_'.$csv_file->getClientOriginalName();
			$destinationPath = 'uploads';
			$csv_file->move($destinationPath,$file_name); //save csv in folder
			
			
			//read csv line by line store in array,
			Csvdata::truncate();
			$file_path = public_path('uploads/'.$file_name);
			$file = fopen($file_path, 'r');
			$data  = array();
			while (($line = fgetcsv($file)) !== FALSE) {
				
				$temp_arr = array();
				$temp_arr['user_id'] = $line[0];
				$temp_arr['basic'] = $line[1];
				$temp_arr['hra'] = $line[2];
				$temp_arr['da'] = $line[3];
				$data[] = $temp_arr;
			  print_r($line);
			}
			fclose($file);
			Salary::insert($data);  //save data in DB
			
			/*Calculate Salaries as per csv*/
				
			//Join Csvdata and Salaries
			

				$sal = \DB::table('salaries')
					->join('csvdata', 'csvdata.user_id', '=', 'salaries.user_id')
					->select('salaries.*', 'csvdata.month', 'csvdata.year', 'csvdata.pay_days')
					->get();
					
				/* Calculate Salary for each employee having salary structure
				 * and no. of working days defined.
				 */	
					
					
					
				$data = array();
				foreach ($sal as $s){

					$temp = array();
					$month = $s->month;
					$year = $s->year;
					$basic = $s->basic;
					$hra = $s->hra;
					$da = $s->da;
					$pay_days = $s->pay_days;
					$month_no = date('m', strtotime($month));
					$days_in_month =  cal_days_in_month(CAL_GREGORIAN,$month_no,$year);
					
					//~ exit;
					$temp['user_id'] = $s->user_id;
					$temp['month'] = $month;
					$temp['year'] = $year;
					$temp['basic'] = ($basic*$pay_days)/$days_in_month;
					$temp['hra'] = ($hra*$pay_days)/$days_in_month;
					$temp['da'] = ($da*$pay_days)/$days_in_month;
					
					$data[] = $temp;
				}
				
				Monthlysalary::insert($data); 		
				
			return redirect('/users');
		} else {
			return view('salary.structure');
		}
    }
    
    
    
    public function process(Request $r)
    {
		$method = $r->method();
		
		if ($method =="POST" || $method =="post"){
			$csv_file = $r->file('csv_file');
			$file_name = $csv_file->getClientOriginalName();
			$destinationPath = 'uploads';
			$csv_file->move($destinationPath,$csv_file->getClientOriginalName()); //save csv in folder
			
			
			//read csv line by line store in array,
			Csvdata::truncate();
			$file_path = public_path('uploads/'.$file_name);
			$file = fopen($file_path, 'r');
			$data  = array();
			while (($line = fgetcsv($file)) !== FALSE) {
				
				$temp_arr = array();
				$temp_arr['user_id'] = $line[0];
				$temp_arr['pay_days'] = $line[1];
				$temp_arr['month'] = $line[2];
				$temp_arr['year'] = $line[3];
				$data[] = $temp_arr;
			  print_r($line);
			}
			fclose($file);
			Csvdata::insert($data);  //save data in DB
			
			/*Calculate Salaries as per csv*/
				
			//Join Csvdata and Salaries
			

				$sal = \DB::table('salaries')
					->join('csvdata', 'csvdata.user_id', '=', 'salaries.user_id')
					->select('salaries.*', 'csvdata.month', 'csvdata.year', 'csvdata.pay_days')
					->get();
					
				$data = array();
				foreach ($sal as $s){
					
					echo "<pre>";
					print_r($s);
					
					
					$temp = array();
					$month = $s->month;
					$year = $s->year;
					$basic = $s->basic;
					$hra = $s->hra;
					$da = $s->da;
					$pay_days = $s->pay_days;
					$month_no = date('m', strtotime($month));
					$days_in_month =  cal_days_in_month(CAL_GREGORIAN,$month_no,$year);
					
					//~ exit;
					$temp['user_id'] = $s->user_id;
					$temp['month'] = $month;
					$temp['year'] = $year;
					$temp['basic'] = ($basic*$pay_days)/$days_in_month;
					$temp['hra'] = ($hra*$pay_days)/$days_in_month;
					$temp['da'] = ($da*$pay_days)/$days_in_month;
					
					$data[] = $temp;
				}
				
				Monthlysalary::insert($data); 		
				
			return redirect('/salaries');
		} else {
			return view('salary.process');
		}
		
    }
    
    
    
    public function salaries(Request $r)
    {
		
		$salaries = \DB::table('monthlysalaries')
            ->leftjoin('users', 'users.id', '=', 'monthlysalaries.user_id')
            ->select('monthlysalaries.*', 'users.name')
            ->paginate(50);
		return view('salary.monthly_salaries', ['monthly_salaries' => $salaries]);
		
    }
}
