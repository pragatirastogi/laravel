<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlysalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('monthlysalaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->decimal('basic', 10, 2);
            $table->decimal('hra', 10, 2);
            $table->decimal('da', 10, 2);
            $table->string('month');
            $table->integer('year');
            $table->timestamps();
        });
        
        
         Schema::table('monthlysalaries', function($table) {
		    $table->foreign('user_id')->references('id')->on('users');
	   });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
