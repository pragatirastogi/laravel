@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="view-users col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Process Salaries</div>
				<div class="panel-body">

		     <form class="form-horizontal" method="POST" action="{{ route('process') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">CSV</label>

                            <div class="col-md-6">
                                <input id="csv_file" type="file" class="form-control" name="csv_file" value="" required autofocus accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Okay
                                </button>
                            </div>
                        </div>
                    </form>

					
				</div>
            </div>
        </div>
    </div>
</div>

@endsection
