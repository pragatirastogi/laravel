@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="view-users col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Monthly Salaries</div>
				<div class="panel-body">
				   <!--div class=""-->
					   @if(!count($monthly_salaries))
							<div class="msg">Salaries not processed yet. <a href="{{ route('process') }}">Click here </a>to process.</div>
					   
					   @else
					   
					   
					   
					   <table class="table table-condensed">
						<thead>
						  <tr>
							<th>User#</th>
							<th>Name</th>
							<th>Year</th>
							<th>Month</th>
							<th>Basic($)</th>
							<th>HRA($)</th>
							<th>DA($)</th>
						  </tr>
						</thead>
						<tbody>
							@foreach ($monthly_salaries as $s)
								<tr>
									<td>{{ $s->user_id }}</td>
									<td>{{ $s->name }}</td>
									<td>{{ $s->year }}</td>
									<td>{{ $s->month }}</td>
									<td>{{ $s->basic }}</td>
									<td>{{ $s->hra }}</td>
									<td>{{ $s->da }}</td>
								 </tr>
								
							@endforeach
						</tbody>
					  </table>
					  @endif
					<!--/div-->

					{{ $monthly_salaries->links() }}
				</div>
            </div>
        </div>
    </div>
</div>


<style>
	table{width:90%}
</style>
@endsection
