@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="menu">
                    <ul>
						<!--li><a href="{{ route('home') }}">Dashboard</a></li-->
						<li><a href="{{ route('users') }}">View Users & Salary Structure</a></li>
						<li><a href="{{ route('salstruct') }}">Upload Salary Structure</a></li>
						<li><a href="{{ route('process') }}">Process Monthly Salaries</a></li>
						<li><a href="{{ route('salaries') }}">View Monthly Salaries</a></li>
                    </ul>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
