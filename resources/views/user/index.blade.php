@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="view-users col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Users</div>
				<div class="panel-body">
				   <!--div class=""-->
					   @if (!count($users))
							<div class="msg">No users added yet.</div>
					   @else
					   <table class="table table-condensed">
						<thead>
						  <tr>
							<th>User#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Basic($)</th>
							<th>HRA($)</th>
							<th>DA($)</th>
						  </tr>
						</thead>
						<tbody>
							@foreach ($users as $user)
								<tr>
									<td>{{ $user->id }}</td>
									<td>{{ $user->name }}</td>
									<td>{{ $user->email }}</td>
									<td>{{ $user->basic }}</td>
									<td>{{ $user->hra }}</td>
									<td>{{ $user->da }}</td>
								 </tr>
								
							@endforeach
						</tbody>
					  </table>
					  @endif
					<!--/div-->

					{{ $users->links() }}
				</div>
            </div>
        </div>
    </div>
</div>


<style>
	table{width:90%}
</style>
@endsection
