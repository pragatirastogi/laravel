<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//~ Route::get('/', function () {
    //~ return view('welcome');
//~ });
Route::get('/', 'HomeController@index')->name('home');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users', 'UserController@index')->name('users');
Route::any('/salstruct', 'SalaryController@salaryStructrue')->name('salstruct');
Route::any('/process', 'SalaryController@process')->name('process');
Route::get('/salaries', 'SalaryController@salaries')->name('salaries');
Route::get('/test', 'SalaryController@test')->name('test');
